import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import '../take_photo.dart';

class PhotoAvatarWidget extends StatefulWidget {
  const PhotoAvatarWidget({super.key});

  @override
  State<PhotoAvatarWidget> createState() => _PhotoAvatarWidgetState();
}

class _PhotoAvatarWidgetState extends State<PhotoAvatarWidget> {
  String? _photo;

  @override
  Widget build(BuildContext context) {
    Widget icono;
    if (_photo == null) {
      icono = IconButton(
        icon: const Icon(Icons.camera_alt),
        onPressed: () async {
          var nav = Navigator.of(context);
          final cameras = await availableCameras();
          final camera = cameras.first;

          var imagePath = await nav.push<String>(
            MaterialPageRoute(
              builder: (context) => TakePhotoPage(camera: camera),
            ),
          );

          if (imagePath != null && imagePath.isNotEmpty) {
            setState(() {
              _photo = imagePath;
            });
          }
        },
      );
    } else {
      icono = CircleAvatar(
        radius: 30,
        backgroundImage: FileImage(File(_photo!)),
      );
    }
    return icono;
  }
}
