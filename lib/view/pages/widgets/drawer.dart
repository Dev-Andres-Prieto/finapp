import 'package:finapp_iniciosesion/controller/login.dart';
import 'package:finapp_iniciosesion/view/pages/iniciosesion.dart';
import 'package:finapp_iniciosesion/view/pages/widgets/photo_avatar.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:finapp_iniciosesion/view/pages/inicio.dart';
import '../pagina_movimientos.dart';

class DrawerWidget extends StatefulWidget {
  const DrawerWidget({super.key});

  @override
  State<DrawerWidget> createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  final _pref = SharedPreferences.getInstance();
  final _loginController = LoginController();
  String _name = "";
  String _email = "";
  bool _isAdmin = false;

  @override
  void initState() {
    super.initState();

    _pref.then((pref) {
      setState(() {
        _name = pref.getString("name") ?? "N/A";
        _email = pref.getString("email") ?? "N/A";
        _isAdmin = pref.getBool("admin") ?? false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
              colors: [
                Color(0xFF4677C6),
                Color(0xFF1F3A66),
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            )),
            child: _header(),
          ),
          ListTile(
            leading: const Icon(Icons.calculate),
            title: const Text('Presupuesto'),
            onTap: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => const Inicio()));
            },
          ),
          ListTile(
            leading: const Icon(Icons.price_change),
            title: const Text('Registros'),
            onTap: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => const VistaGastos()));
            },
          ),
          if (_isAdmin)
            ListTile(
              leading: const Icon(Icons.insert_chart),
              title: const Text('Informes'),
              onTap: () {},
            ),
          ListTile(
            leading: const Icon(Icons.exit_to_app_rounded),
            title: const Text('Cerrar sesión'),
            onTap: () async {
              var nav = Navigator.of(context);

              _loginController.logoutUser();
              var pref = await _pref;
              pref.remove("uid");
              pref.remove("name");
              pref.remove("email");
              pref.remove("admin");

              nav.pushReplacement(
                MaterialPageRoute(builder: (context) => InicioSesion()),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _header() {
    const image = PhotoAvatarWidget();
    return Row(
      children: [
        const CircleAvatar(
          radius: 30,
          child: image,
        ),
        const SizedBox(width: 8),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                _name,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
              const SizedBox(height: 8),
              Text(
                _email,
                style: const TextStyle(
                  color: Colors.white,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
