import 'package:finapp_iniciosesion/view/pages/registrousuario.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../controller/login.dart';
import '../../controller/request/login.dart';
import 'inicio.dart';
import 'package:flutter/material.dart';

class InicioSesion extends StatelessWidget {
  final _pref = SharedPreferences.getInstance();
  final _imageUrlLogo = "assets/images/logo.png";
  final _imageUrlFb = "assets/images/fb.png";
  final _imageUrlGoogle = "assets/images/google.png";
  late LoginRequest _request;
  late LoginController _controller;

  InicioSesion({super.key}) {
    _controller = LoginController();
    _request = LoginRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
          colors: [
            Color(0xFF4677C6),
            Color(0xFF1F3A66),
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        )),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(
                left: 30.0, top: 5.0, right: 30.0, bottom: 10.0),
            child: Column(
              children: <Widget>[
                _logo(),
                _formulario(context),
                _inicioAlternativo(),
                TextButton(
                  style: TextButton.styleFrom(
                    textStyle:
                        const TextStyle(fontSize: 16, color: Colors.blue),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => RegistroUsuario(),
                      ),
                    );
                  },
                  child: const Text("Regístrate aquí"),
                ),
                const SizedBox(
                  height: 27,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _logo() {
    return Column(
      children: [
        SizedBox(width: 250, height: 250, child: Image.asset(_imageUrlLogo)),
      ],
    );
  }

  Widget _formulario(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    return Form(
      key: formKey,
      child: Column(
        children: [
          SizedBox(child: _campoCorreoElectronico()),
          const SizedBox(
            height: 20,
          ),
          SizedBox(child: _campoPassword()),
          const SizedBox(
            height: 60,
          ),
          SizedBox(
              height: 40, width: 450, child: _iniciarSesion(context, formKey)),
          const SizedBox(
            height: 30,
          ),
          //_inicioAlternativo(),
        ],
      ),
    );
  }

  Widget _campoCorreoElectronico() {
    return TextFormField(
        keyboardType: TextInputType.emailAddress,
        decoration: const InputDecoration(
          icon: Icon(Icons.email_outlined, size: 35),
          hintText: "user@email.com",
          fillColor: Colors.white,
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El correo electrónico es obligatorio";
          }
          if (!value.contains("@") || !value.contains(".")) {
            return "El correo tiene un formato inválido";
          }
          return null;
        },
        onSaved: (value) {
          _request.email = value!;
        });
  }

  Widget _campoPassword() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      obscureText: true,
      decoration: const InputDecoration(
        icon: Icon(Icons.key_off_outlined, size: 35),
        hintText: "Password",
        fillColor: Colors.white,
        filled: true,
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El password es obligatorio";
        }
        return null;
      },
      onSaved: (value) {
        _request.password = value!;
      },
    );
  }

  Widget _inicioAlternativo() {
    return Column(
      children: [
        const Text("Ingresa con tu cuenta de:",
            style: TextStyle(fontSize: 16, color: Colors.blue)),
        const SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(
              icon: Image.asset(_imageUrlFb),
              iconSize: 35,
              onPressed: () {},
            ),
            IconButton(
              icon: Image.asset(_imageUrlGoogle),
              iconSize: 35,
              onPressed: () {},
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget _iniciarSesion(BuildContext context, GlobalKey<FormState> formKey) {
    return ElevatedButton(
      child: const Text("INICIAR SESIÓN"),
      onPressed: () async {
        if (formKey.currentState!.validate()) {
          formKey.currentState!.save();

          try {
            final nav = Navigator.of(context);
            var userInfo = await _controller.validateEmailPassword(_request);

            var pref = await _pref;
            pref.setString("uid", userInfo.id!);
            pref.setString("name", userInfo.name!);
            pref.setString("email", userInfo.email!);
            pref.setBool("admin", userInfo.isAdmin!);

            nav.pushReplacement(MaterialPageRoute(
              builder: (context) => const Inicio(),
            ));
          } catch (e) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(e.toString())));
          }
        }
      },
    );
  }
}
