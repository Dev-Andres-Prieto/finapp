import 'package:flutter/material.dart';
import 'package:finapp_iniciosesion/controller/login.dart';
import 'package:finapp_iniciosesion/view/pages/iniciosesion.dart';
import '../../controller/request/registrousuario.dart';

import 'widgets/checkbox.dart';

class RegistroUsuario extends StatefulWidget {
  late RegistroUsuarioRequest _data;
  late LoginController _loginController;

  RegistroUsuario({super.key}) {
    _loginController = LoginController();
    _data = RegistroUsuarioRequest();
  }

  @override
  State<RegistroUsuario> createState() => _RegistroUsuarioState();
}

class _RegistroUsuarioState extends State<RegistroUsuario> {
  final _imageUrl = "assets/images/logo.png";
  var _confirmpassword;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
          colors: [
            Color(0xFF4677C6),
            Color(0xFF1F3A66),
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        )),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(
                left: 30.0, top: 5.0, right: 30.0, bottom: 10.0),
            child: Column(
              children: <Widget>[
                _logo(),
                _formulario(context),
                TextButton(
                  style: TextButton.styleFrom(
                    textStyle:
                        const TextStyle(fontSize: 16, color: Colors.blue),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => InicioSesion(),
                      ),
                    );
                  },
                  child: const Text("Iniciar sesión"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _logo() {
    return Column(
      children: [
        SizedBox(width: 250, height: 250, child: Image.asset(_imageUrl)),
      ],
    );
  }

  Widget _formulario(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    return Form(
      key: formKey,
      child: Column(
        children: [
          SizedBox(child: _campoNombre()),
          const SizedBox(
            height: 20,
          ),
          SizedBox(child: _campoCorreoElectronico()),
          const SizedBox(
            height: 20,
          ),
          SizedBox(child: _campoPassword()),
          const SizedBox(
            height: 20,
          ),
          SizedBox(child: _campoConfirmPassword()),
          const SizedBox(
            child: CheckboxFormField(),
          ),
          SizedBox(
              width: 450,
              child: ElevatedButton(
                  child: const Text("REGISTRARME"),
                  onPressed: () async {
                    if (formKey.currentState!.validate()) {
                      formKey.currentState!.save();

                      try {
                        await widget._loginController
                            .registerNewUser(widget._data);

                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                              content:
                                  Text("Usuario registrado corréctamente")),
                        );

                        Navigator.pop(context);
                      } catch (error) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(error.toString()),
                          ),
                        );
                      }

                      print(widget._data);
                    }
                  })),
          const SizedBox(
            height: 9,
          ),
        ],
      ),
    );
  }

  Widget _campoNombre() {
    return TextFormField(
        keyboardType: TextInputType.name,
        decoration: const InputDecoration(
          icon: Icon(Icons.person_add, size: 35),
          hintText: "Nombres y Apellidos",
          fillColor: Colors.white,
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "Los nombres y apellidos son obligatorios";
          }
          return null;
        },
        onSaved: (value) {
          widget._data.name = value!;
        });
  }

  Widget _campoCorreoElectronico() {
    return TextFormField(
        keyboardType: TextInputType.emailAddress,
        decoration: const InputDecoration(
          icon: Icon(Icons.email_outlined, size: 35),
          hintText: "user@email.com",
          fillColor: Colors.white,
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El correo electrónico es obligatorio";
          }
          if (!value.contains("@") || !value.contains(".")) {
            return "El correo tiene un formato inválido";
          }
          return null;
        },
        onSaved: (value) {
          widget._data.email = value!;
        });
  }

  Widget _campoPassword() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      obscureText: true,
      decoration: const InputDecoration(
        icon: Icon(Icons.key_off_outlined, size: 35),
        hintText: "Password",
        fillColor: Colors.white,
        filled: true,
      ),
      validator: (value) {
        _confirmpassword = value;
        if (value == null || value.isEmpty) {
          return "El password es obligatorio";
        }
        return null;
      },
      onSaved: (value) {
        widget._data.password = value!;
      },
    );
  }

  Widget _campoConfirmPassword() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      obscureText: true,
      decoration: const InputDecoration(
        icon: Icon(Icons.key_off_outlined, size: 35),
        hintText: "Confirmar password",
        fillColor: Colors.white,
        filled: true,
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Confirmar el password es obligatorio";
        }
        if (value != _confirmpassword) {
          return "Los passwords no son iguales";
        }
        return null;
      },
    );
  }
}
