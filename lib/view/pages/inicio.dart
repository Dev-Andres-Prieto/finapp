import 'package:finapp_iniciosesion/view/pages/widgets/drawer.dart';
import 'package:flutter/material.dart';

class Inicio extends StatelessWidget {
  const Inicio({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*appBar: AppBar(
        title: const Text("Inicio"),
      ),*/
      appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.all(8.0),
                child: const Text('Inicio'),
              ),
              const SizedBox(
                width: 180,
              ),
              Image.asset(
                "assets/images/logo.png",
                fit: BoxFit.contain,
                height: 60,
              ),
            ],
          ),
          flexibleSpace: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: <Color>[Color(0xFF4677C6), Color(0xFF1F3A66)])),
          )),
      drawer: const DrawerWidget(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(8.0),
        child: WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: const <Text>[
                  Text("Presupuesto"),
                ],
              ),
              const SizedBox(
                width: 30,
              ),
              Column(
                children: const <Text>[
                  Text("Registro"),
                ],
              ),
              const SizedBox(
                width: 30,
              ),
              Column(
                children: const <Text>[
                  Text("Informes"),
                ],
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.yellow,
        foregroundColor: const Color.fromARGB(255, 1, 38, 68),
        onPressed: () {},
        child: const Icon(Icons.add),
      ),
    );
  }
}
