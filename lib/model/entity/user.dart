import 'package:cloud_firestore/cloud_firestore.dart';

class UserEntity {
  late String? id;
  late String? name;
  late String? email;
  late bool? isAdmin;

  UserEntity({this.name, this.email, this.isAdmin});

  factory UserEntity.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options) {
    var data = snapshot.data();

    return UserEntity(
        name: data?["name"], email: data?["email"], isAdmin: data?["isAdmin"]);
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (name != null && name!.isNotEmpty) "name": name,
      if (email != null && name!.isNotEmpty) "email": email,
      //"isAdmin" : isAdmin == null ? false : isAdmin : isAdmin;
      "isAdmin": isAdmin ?? false
    };
  }

  @override
  String toString() {
    return "UserEntity {$name, $email, $isAdmin}";
  }
}
