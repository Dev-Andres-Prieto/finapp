import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import '../model/objetos.dart';

class DatabaseHandler {
  Future<Database> initializeDB() async {
    String path = await getDatabasesPath();
    return openDatabase(
      join(path, 'finapp.db'),
      onCreate: (database, version) async {
        await database.execute(
          "CREATE TABLE finanzas(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, valor TEXT NOT NULL, tipogasto TEXT NOT NULL, movimiento TEXT)",
        );
      },
      version: 1,
    );
  }

  Future<int> insertGasto(List<Objeto> GASTOS) async {
    int result = 0;
    final Database db = await initializeDB();
    for (var _gasto in GASTOS) {
      result = await db.insert('finanzas', _gasto.toMap());
    }
    return result;
  }

  Future<List<Objeto>> obtenerMovimientos() async {
    final Database db = await initializeDB();
    final List<Map<String, Object?>> queryResult = await db.query('finanzas');
    return queryResult.map((e) => Objeto.fromMap(e)).toList();
  }

  Future<void> borrarMovimiento(int id) async {
    final db = await initializeDB();
    await db.delete(
      'finanzas',
      where: "id = ?",
      whereArgs: [id],
    );
  }
}
