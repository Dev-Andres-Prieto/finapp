import 'package:finapp_iniciosesion/view/pages/inicio.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'firebase_options.dart';
import 'view/pages/iniciosesion.dart';

void main(List<String> args) async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const FinApp());
}

class FinApp extends StatefulWidget {
  const FinApp({super.key});

  @override
  State<FinApp> createState() => _FinAppState();
}

class _FinAppState extends State<FinApp> {
  final _pref = SharedPreferences.getInstance();
  Widget _init = const Scaffold(
    body: Center(
      child: CircularProgressIndicator(),
    ),
  );

  @override
  void initState() {
    super.initState();
    _pref.then((pref) {
      setState(() {
        if (pref.getString("uid") != null) {
          _init = const Inicio();
        } else {
          _init = InicioSesion();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "FinApp",
      home: _init,
    );
  }
}
