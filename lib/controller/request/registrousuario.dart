class RegistroUsuarioRequest {
  late String name;
  late String email;
  late String password;

  @override
  String toString() {
    return "$name, $email, $password";
  }
}
