class UserInfoResponse {
  late String? id;
  late String? name;
  late String? email;
  late bool? isAdmin;

  UserInfoResponse({this.id, this.name, this.email, this.isAdmin});
}
